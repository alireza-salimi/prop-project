from django.db import models
from django.utils.translation import ugettext_lazy as _


class TypeOfDamage(models.IntegerChoices):
    DEVICES = (1, _('Devices'))


class Status(models.IntegerChoices):
    TODO = (1, _('TODO'))
    IN_PROGRESS = (2, _('In progress'))
    DONE = (3, _('Done'))


class DamageReport(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='damage_reports', verbose_name=_('User'))
    billing_id = models.CharField(max_length=100, verbose_name=_('Billing ID'))
    last_bill_image = models.ImageField(upload_to='images/', verbose_name=_('Last bill image'), blank=True, null=True)
    national_card_image = models.ImageField(upload_to='images/', verbose_name=_('National card image'), blank=True, null=True)
    ownership_doc_image = models.ImageField(upload_to='images/', verbose_name=_('Ownership document image'), blank=True, null=True)
    date = models.DateField(verbose_name=_('Date'))
    time = models.TimeField(verbose_name=_('Time'))
    type_of_damage = models.IntegerField(choices=TypeOfDamage.choices, default=TypeOfDamage.DEVICES, verbose_name=_('Type of damage'))
    description = models.TextField(verbose_name=_('Description'))
    amount_of_damages = models.FloatField(verbose_name=_('Amount of damages'))
    status = models.IntegerField(choices=Status.choices, default=Status.TODO, verbose_name=_('Status'))


class AdditionalDocument(models.Model):
    image = models.ImageField(upload_to='images/', verbose_name=_('Image'), blank=True, null=True)
    damage_report = models.ForeignKey(
        DamageReport, on_delete=models.CASCADE, related_name='additional_documents', verbose_name=_('Damage report')
    )
