from damage.models import DamageReport
from damage.serializers import CreateDamageReportSerializer
from rest_framework.generics import CreateAPIView


class CreateDamageReportView(CreateAPIView):
    serializer_class = CreateDamageReportSerializer
    queryset = DamageReport.objects.all()
