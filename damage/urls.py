from django.urls import path
from .views import CreateDamageReportView


urlpatterns = [
    path('', CreateDamageReportView.as_view(), name='damage-reports')
]