from django.contrib import admin
from .models import DamageReport, AdditionalDocument


class AdditionalDocumentInline(admin.TabularInline):
    model = AdditionalDocument


class DamageReportAdmin(admin.ModelAdmin):
    list_display = ['user', 'billing_id']
    inlines = [AdditionalDocumentInline]


admin.site.register(DamageReport, DamageReportAdmin)
